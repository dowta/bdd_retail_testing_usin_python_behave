Feature: PricedAvail oneWay

   As a UI developer
   I want to have priced availabilities
   for TX airlines

  
        Scenario: Priced availability

            Given I have an origin "<origin>"

              And I have a destination "<destination>"

             When I send a priced Avail request

             Then I must have a 200 status

     
                  |origin|destination|
                  |ORY   |    PTP    |
                  |ORY   |    FDF    |
                  |ORY   |    PPT    |



        Scenario: Fare benefits
        
            Given an origin "<origin>"

              And  a destination "<destination>"

             When I send a pricedAvail request

             Then I must receive a pricedAvail response
          
              And I must receive the fare benefits included in the pricedAvail response


        Scenario:  ESMART210 benefits : Ticket change

            Given a successfull pricedAvail request

             When the cabin is  "ECONOMY" Fare Family is EBASIC210

             Then ticket change must be chargeable

              And must cost 150€
              

             