Feature: Protect API with recaptcha


        Scenario: valid recaptcha token  in pricedAvail

            Given a pricedAvail request

             When I set the recaptcha in the headers
             
              And I send The pricedAvail request

             Then my request must not be rejected



        Scenario: valid recaptcha token  in prebook

            Given a successfull pricedAvail request

             When I set the recaptcha in the headers
             
              And I send The pricedAvail request

             Then my request must not be rejected
             


        Scenario: valid recaptcha token in retrieve

            Given a bookingId

             When I set the recaptcha in the headers
             
              And I send The pricedAvail request

             Then my request must not be rejected



        Scenario: invalid recaptcha token  in pricedAvail

            Given a pricedAvail request

             When I set the recaptcha in the headers
             
              And I send The pricedAvail request

             Then my request must be rejected



        Scenario: invalid recaptcha token  in prebook

            Given a successfull pricedAvail request

             When I set the recaptcha in the headers
             
              And I send The pricedAvail request

             Then my request must be rejected



        Scenario: invalid recaptcha token in retrieve

            Given a bookingId

             When I set the recaptcha in the headers
             
              And I send The pricedAvail request

             Then my request must be rejected