

from behave import given,when,then,step # pylint: disable=no-name-in-module
import requests
import json
import sys
import yaml

with open("/home/dowta/workspace/retail_BDD_Test/configuration/basic.yml", 'r') as stream:
    try:
       cfg= yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

@given(u'I have an origin "{origin}"')

def set_origin(context,origin):
    
    context.origin= origin
    context.one_way_request= {
       
       "tripType": "OneWay",
       "travelers": {
        "numberOfAdult": 1,
        "numberOfChild": 0,
        "numberOfInfant": 0,
        "numberOfSenior": 0,
        "numberOfYouth": 0
    },
       "outbound": {
        "origin": "{}".format('ORY'),
        "destination": "",
        "startDate": "2020-05-13"
    }

    }
    
@step(u'I have a destination "{destination}"')

def set_destination(context,destination):
    context.destination=destination
    context.one_way_request= {
       
       "tripType": "OneWay",
       "travelers": {
        "numberOfAdult": 1,
        "numberOfChild": 0,
        "numberOfInfant": 0,
        "numberOfSenior": 0,
        "numberOfYouth": 0
    },
       "outbound": {
        "origin": "{}".format('ORY'),
        "destination": "{}".format('PTP'),
        "startDate": "2020-05-13"
    }

    }
    

@when(u'I send a priced Avail request')
def send_request(context):
    user = cfg['tapi-credentials']['user']
    password= cfg['tapi-credentials']['password']
    url = 'https://tapi-api-retail.test.futureoftravel.eu/{}'.format('pricedAvail')
    header={'Content-Type':'application/json','Accept':'application/json','Taer-Client-Code':'FWI','Taer-Point-Of-Sale':'FR'}
    context.response= requests.post(url,data=json.dumps(context.one_way_request),headers=header,auth=(user,password))

@then(u'I must have a 200 status')
def check_result(context) :
   print(context.response)
   assert context.response.status_code == 200






