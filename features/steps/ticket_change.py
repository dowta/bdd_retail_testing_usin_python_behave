from behave import given,when,then,step# pylint: disable=no-name-in-module
import requests
import json
import sys
import yaml

with open("/home/dowta/workspace/retail_BDD_Test/configuration/basic.yml", 'r') as stream:
    try:
       cfg= yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)



@given(u'a successfull pricedAvail request')
def send_request(context):
  context.one_way_request= {
       
       "tripType": "OneWay",
       "travelers": {
        "numberOfAdult": 1,
        "numberOfChild": 0,
        "numberOfInfant": 0,
        "numberOfSenior": 0,
        "numberOfYouth": 0
    },
       "outbound": {
        "origin": "{}".format('ORY'),
        "destination": "{}".format('PTP'),
        "startDate": "2020-05-13"
    }

    }
  user= cfg['tapi-credentials']['user']
  password= cfg['tapi-credentials']['password']
  url = 'https://tapi-api-retail.test.futureoftravel.eu/{}'.format('pricedAvail')
  header={'Content-Type':'application/json','Accept':'application/json','Taer-Client-Code':'FWI','Taer-Point-Of-Sale':'FR'}
  context.response= requests.post(url,data=json.dumps(context.one_way_request),headers=header,auth=(user,password))

@when(u'the cabin is  "ECONOMY"')
def return_Cabin_Fare_Benefits(context):
    for cabin in context.response.json()["data"]["cabins"]:
        if cabin["code"]=='ECONOMY':
             context.cabin = cabin
    
    for fareFamily in context.cabin["fareFamilies"]:
      if fareFamily["code"]=='EBASIC210':
         context.fareFamily = fareFamily["fareBenefits"]
    

@then(u'ticket change must be chargeable')
def choose_a_benefit(context):
    for benef in context.fareFamily:
         if benef["code"]== 'TICKET_CHANGE':
             context.benef=benef
             assert benef['inclusionStatus'] == 'CHARGEABLE'

@step(u'must cost 150€')
def check_value(context):
    assert context.benef['value']=='150€'


  